package com.example.johncarter.ethelon;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    Toolbar toolbar;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.landing_page_toolbar);
        toolbar.setTitle("Settings");
        ((LandingPage)getActivity()).setToolbarWithBack(toolbar);

        return view;
    }

}
