package com.example.johncarter.ethelon;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class splashScreen extends AppCompatActivity {

    String fontPath = "fonts/Milkshake.ttf";

    ImageView mDotLeft, mDotright, mLogoE;
    TextView e, thelon;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        init();
        startAnimation();

    }

    private void startAnimation() {
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.clockwise);
        mLogoE.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                e.startAnimation(animation);
                thelon.startAnimation(animation);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mDotLeft.animate().scaleX((float) .7).scaleY((float) .7).setDuration(100).start();
                mDotLeft.setScaleX((float) .7);
                mDotLeft.setScaleY((float) .7);
                mDotright.animate().scaleY((float) .7).scaleX((float) .7).setDuration(100).setStartDelay(200).start();

                Thread splashThread = new Thread(){

                    @Override
                    public void run() {
                        try {
                            int waited = 0;
                            while(waited < 2200){
                                sleep(100);
                                waited+=100;
                            }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                            Pair<View, String> p1 = Pair.create((View) mLogoE, "letterE");
                                            Pair<View, String> p2 = Pair.create((View) mDotLeft, "dotLeft");
                                            Pair<View, String> p3 = Pair.create((View) mDotright, "dotRight");
                                            Pair<View, String> p4 = Pair.create((View) e, "e");
                                            Pair<View, String> p5 = Pair.create((View) thelon, "thelon");
                                            ActivityOptionsCompat options = ActivityOptionsCompat.
                                                    makeSceneTransitionAnimation(splashScreen.this, p1, p2, p3, p4, p5);
                                            startActivity(intent, options.toBundle());
                                        }
                                        else {
                                            startActivity(intent);
                                            finish();
                                        }

                                    }
                                });
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                };
                splashThread.start();

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void init() {
        e = (TextView) findViewById(R.id.e);
        thelon = (TextView) findViewById(R.id.thelon);
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        thelon.setTypeface(tf);
        e.setTypeface(tf);

        mLogoE = (ImageView) findViewById(R.id.imageView);
        mDotLeft = (ImageView) findViewById(R.id.imageView2);
        mDotright = (ImageView) findViewById(R.id.imageView3);

    }
}
