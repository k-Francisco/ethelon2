package com.example.johncarter.ethelon;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    String fontPath = "fonts/Milkshake.ttf";
    EditText mEmail, mPassword;
    Button mLogin, mLoginWFb, mLoginWGoogle;
    LinearLayout divider;
    Animation animation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        startanimation();


    }

    private void startanimation() {

        mEmail.animate().translationY(0).setInterpolator(new OvershootInterpolator()).setDuration(1000).start();
        mPassword.animate().translationY(0).setInterpolator(new OvershootInterpolator()).setStartDelay(200).setDuration(1000).start();
        mLogin.animate().translationY(0).setInterpolator(new OvershootInterpolator()).setStartDelay(400).setDuration(1000).start();
        divider.animate().translationY(0).setInterpolator(new OvershootInterpolator()).setStartDelay(600).setDuration(1000).start();
        mLoginWFb.animate().translationY(0).setInterpolator(new OvershootInterpolator()).setStartDelay(800).setDuration(1000).start();
        mLoginWGoogle.animate().translationY(0).setInterpolator(new OvershootInterpolator()).setStartDelay(1000).setDuration(1000).start();

    }

    private void init() {

        mEmail = (EditText) findViewById(R.id.etEmail);
        mPassword = (EditText) findViewById(R.id.etPassword);

        mLogin = (Button) findViewById(R.id.btnLogin);
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LandingPage.class);
                startActivity(intent);
                finish();
            }
        });
        mLoginWFb = (Button) findViewById(R.id.btnLoginWFb);
        mLoginWGoogle = (Button) findViewById(R.id.btnLoginWGoogle);

        divider = (LinearLayout) findViewById(R.id.dividerOr);

        TextView e = (TextView) findViewById(R.id.e);
        TextView thelon = (TextView) findViewById(R.id.thelon);
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        thelon.setTypeface(tf);
        e.setTypeface(tf);

        mPassword.setTypeface(Typeface.DEFAULT);
        mPassword.setTransformationMethod(new PasswordTransformationMethod());

    }
}


