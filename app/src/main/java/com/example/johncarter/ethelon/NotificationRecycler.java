package com.example.johncarter.ethelon;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by john carter on 1/28/2017.
 */

public class NotificationRecycler extends RecyclerView.Adapter<NotificationRecycler.ViewHolder> {


    private String name[] = {
            "Kristian Francisco",
            "Kristian Francisco",
            "Kristian Francisco",
            "Kristian Francisco",
            "Kristian Francisco",
            "Kristian Francisco",
            "Kristian Francisco",
            "Kristian Francisco",
    };

    private int pictures[] = {
            R.drawable.cat_1,
            R.drawable.cat_1,
            R.drawable.cat_1,
            R.drawable.cat_1,
            R.drawable.cat_1,
            R.drawable.cat_1,
            R.drawable.cat_1,
            R.drawable.cat_1,

    };


    @Override
    public NotificationRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_recycler_layout,parent,false);
        NotificationRecycler.ViewHolder viewHolder = new NotificationRecycler.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NotificationRecycler.ViewHolder holder, int position) {

        holder.mNotificationText.setText(name[position]);
        holder.mNotificationPic.setImageResource(pictures[position]);
    }

    @Override
    public int getItemCount() {
        return name.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mNotificationPic;
        private TextView mNotificationText;

        public ViewHolder(View itemView) {
            super(itemView);

            mNotificationText = (TextView) itemView.findViewById(R.id.notificationText);
            mNotificationPic = (ImageView) itemView.findViewById(R.id.notificationPic);


        }
    }
}
