package com.example.johncarter.ethelon;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by john carter on 1/26/2017.
 */

public class LeaderboardRecycler extends RecyclerView.Adapter<LeaderboardRecycler.ViewHolder> {


    private String name[] = {
            "Kristian Francisco",
            "Kristian Francisco",
            "Kristian Francisco",
            "Kristian Francisco",
            "Kristian Francisco",
            "Kristian Francisco",
            "Kristian Francisco",
            "Kristian Francisco",
    };

    private int pictures[] = {
            R.drawable.cat_1,
            R.drawable.cat_1,
            R.drawable.cat_1,
            R.drawable.cat_1,
            R.drawable.cat_1,
            R.drawable.cat_1,
            R.drawable.cat_1,
            R.drawable.cat_1,

    };



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.leaderboard_recycler_layout,parent,false);
        LeaderboardRecycler.ViewHolder viewHolder = new LeaderboardRecycler.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.mVolunteerName.setText(name[position]);
        holder.mVolunteerPicture.setImageResource(pictures[position]);

    }

    @Override
    public int getItemCount() {
        return name.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mVolunteerPicture;
        private TextView mVolunteerName;

        public ViewHolder(View itemView) {
            super(itemView);

            mVolunteerName = (TextView) itemView.findViewById(R.id.volunteerName);
            mVolunteerPicture = (ImageView) itemView.findViewById(R.id.volunteerPicture);

        }
    }
}
