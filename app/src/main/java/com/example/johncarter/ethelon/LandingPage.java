package com.example.johncarter.ethelon;

import android.support.v4.app.Fragment;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import com.heinrichreimersoftware.materialdrawer.DrawerActivity;
import com.heinrichreimersoftware.materialdrawer.structure.DrawerItem;
import com.heinrichreimersoftware.materialdrawer.structure.DrawerProfile;
import com.heinrichreimersoftware.materialdrawer.theme.DrawerTheme;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;


public class LandingPage extends DrawerActivity {


    Toolbar toolbar;
    SpaceNavigationView spaceNavigationView;
    final int CENTER_ITEM_INDEX = 2;
    private static LeaderBoardFragment leaderBoardFragment;
    private static NotificationFragment notificationFragment;
    private static SettingsFragment settingsFragment;
    private static ProfileFragment profileFragment;
    private static Fragment stackedFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);

        toolbar = (Toolbar) findViewById(R.id.landing_page_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        //initializing the bottombar
        initBottomBar(savedInstanceState);

        //initializing the drawer
        initDrawer();

        //initializing the search bar
        initSearchBar();

        leaderBoardFragment = new LeaderBoardFragment();
        notificationFragment = new NotificationFragment();
        settingsFragment = new SettingsFragment();
        profileFragment = new ProfileFragment();
        stackedFragment = leaderBoardFragment;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle toolbar item clicks here. It'll
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    private void initSearchBar() {

    }

    private void initDrawer() {

        //setting the background and primary/secondary text of the drawer
        setDrawerTheme(
                new DrawerTheme(this)
                    .setBackgroundColorRes(R.color.drawer_background)
                    .setTextColorPrimaryRes(R.color.drawer_text_primary)
                    .setTextColorSecondaryRes(R.color.drawer_text_secondary)
                    .setHighlightColorRes(R.color.drawer_highlight)
        );

        //adding profile on the drawer
        addProfile(
                new DrawerProfile()
                    .setId(1)
                    .setRoundedAvatar((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.cat_1))
                    .setBackground(ContextCompat.getDrawable(this, R.drawable.home_backpic))
                    .setName("Kristian Francisco")
                    .setDescription("piattosnovalays@gmail.com")
        );

        //adding items above the divider on the drawer
        addItems(
                new DrawerItem()
                        .setTextPrimary("Profile")
                        .setImage(ContextCompat.getDrawable(this, R.drawable.ic_action_avatar)),
                new DrawerItem()
                        .setTextPrimary("Performance")
                        .setImage(ContextCompat.getDrawable(this, R.drawable.ic_action_ribbon_badge_award)),
                new DrawerItem()
                        .setTextPrimary("Settings")
                        .setImage(ContextCompat.getDrawable(this, R.drawable.ic_action_settings)),
                new DrawerItem()
                        .setTextPrimary("Log out")
                        .setImage(ContextCompat.getDrawable(this, R.drawable.ic_action_log_out))
        );

        //adding a divider
        addDivider();

        //adding items below the divider on the drawer
        addItems(
                new DrawerItem()
                        .setTextPrimary("Partnered Foundations")
                        .setImage(ContextCompat.getDrawable(this, R.drawable.ic_action_partnership)),
                new DrawerItem()
                        .setTextPrimary("Help & Feedback")
                        .setImage(ContextCompat.getDrawable(this, R.drawable.ic_action_question)),
                new DrawerItem()
                        .setTextPrimary("Report Bug")
                        .setImage(ContextCompat.getDrawable(this, R.drawable.ic_action_bug_report_button)),
                new DrawerItem()
                        .setTextPrimary("About Ethelon")
                        .setImage(ContextCompat.getDrawable(this, R.drawable.ic_action_keyboard_key_e))
        );

        //handle drawer clicks
        setOnItemClickListener(new DrawerItem.OnItemClickListener() {
            @Override
            public void onClick(DrawerItem drawerItem, long id, int position) {
                selectItem(position);
                closeDrawer();
                switch (position){

                    case 0://onClicked Profile
                        if(!profileFragment.isAdded()){
                            addFragment(R.id.fragment_space, profileFragment);
                        }
                        else{
                            replaceFragment(R.id.fragment_space, profileFragment);
                        }
                        toggleBottombarVisibility(true);
                        selectItem(9);
                        break;
                    case 1:
                        break;
                    case 2://onClicked Settings
                        if(!settingsFragment.isAdded()){
                            addFragment(R.id.fragment_space, settingsFragment);
                        }
                        else{
                            replaceFragment(R.id.fragment_space, settingsFragment);
                        }
                        toggleBottombarVisibility(true);
                        selectItem(9);

                        break;
                    case 3:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
                        break;
                    case 8:
                        break;

                }

            }
        });
    }

    private void initBottomBar(Bundle savedInstanceState) {

        //bottombar interface
        spaceNavigationView = (SpaceNavigationView) findViewById(R.id.spaceBottomBar);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("Notifications",R.drawable.ic_action_alarm_bell));
        spaceNavigationView.addSpaceItem(new SpaceItem("LeaderBoard",R.drawable.ic_action_podium_1));
        spaceNavigationView.setCentreButtonRippleColor(R.color.drawer_highlight);
        spaceNavigationView.setCentreButtonSelectable(true);
        spaceNavigationView.setCentreButtonIconColorFilterEnabled(false);
        spaceNavigationView.setCentreButtonSelected();


        //onclick listeners for bottombar
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {

            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex){
                    case 0:
                        stackedFragment = notificationFragment;
                        if(!notificationFragment.isAdded()) {
                            addFragment(R.id.fragment_space, notificationFragment);
                        }
                        else{
                            replaceFragment(R.id.fragment_space, notificationFragment);
                        }
                        toggleBottombarVisibility(false);
                        break;

                    case 1:
                        stackedFragment = leaderBoardFragment;
                        if(!leaderBoardFragment.isAdded()){
                            addFragment(R.id.fragment_space, leaderBoardFragment);
                        }
                        else{
                            replaceFragment(R.id.fragment_space, leaderBoardFragment);
                        }
                        toggleBottombarVisibility(false);
                        break;
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {

            }
        });
    }





    //adding fragments
    public void addFragment(int container, Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .add(container, fragment).commit();
    }

    //replacing fragments
    public void replaceFragment(int container, Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .replace(container, fragment).commit();
    }

    //toggle bottombar visibility
    public void toggleBottombarVisibility(boolean wipe){
        if(wipe == true && spaceNavigationView.getVisibility() == View.VISIBLE)
            spaceNavigationView.setVisibility(View.GONE);
        else if(wipe == false && spaceNavigationView.getVisibility() == View.GONE)
            spaceNavigationView.setVisibility(View.VISIBLE);
    }

    //toolbar collapsible in leaderboard
    public void setToolbarLeaderboard(Toolbar toolbar){

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

    }

    //toolbar with back button
    public void setToolbarWithBack(Toolbar toolbar){
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(stackedFragment != null){
                    replaceFragment(R.id.fragment_space, stackedFragment);
                    toggleBottombarVisibility(false);
                }
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


}
